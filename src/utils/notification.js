import { notification } from "antd";
const openNotificationWithIcon = (type, title, desc) => {
    console.log("type", type)
    notification[type]({
        message: title,
        description: desc
    });
};
export default openNotificationWithIcon;