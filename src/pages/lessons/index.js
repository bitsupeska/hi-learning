import { Card } from "antd";
import Layout, { Content } from "antd/lib/layout/layout";
import { useContext, useEffect } from "react";
import { LessonContext } from "../../context/LessonContext";
import LessonsList from "./components/lessons_list";
import VideoPlayer from "./components/video";
import ClipLoader from "react-spinners/ClipLoader";

const Lessons = () => {
    const course = JSON.parse(localStorage.getItem("watchCourse"));
    const lessonContext = useContext(LessonContext);
    useEffect(() => {
        lessonContext.getLessons(course.id);
    }, [])
    return <Content style={{ padding: '0 50px', height: "100vh" }} ><Card>
        <div>

            {
                lessonContext.state.loading ?
                    <div
                        className="site-layout-background"
                        style={{
                            padding: 24, textAlign: 'center',
                            justifyContent: "center",
                            display: "flex",
                            paddingBottom: "2%"
                        }}>
                        <ClipLoader size={50} color={"#1890ff"}></ClipLoader>
                    </div> :
                    <>
                        <Layout className="site-layout">
                            <div className="site-layout-background"
                                style={{
                                    padding: 24,
                                    textAlign: 'center',
                                    justifyContent: "center",
                                    display: "flex",
                                    paddingBottom: "2%"
                                }}>
                                <VideoPlayer ></VideoPlayer>
                            </div>
                        </Layout>

                        <LessonsList course={course}></LessonsList>
                    </>
            }
        </div>
    </Card>
    </Content>
}

export default Lessons;