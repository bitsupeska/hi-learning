import { List } from "antd"
import Avatar from "antd/lib/avatar/avatar"
import { useContext } from "react";
import { LessonContext } from "../../../../context/LessonContext";
import classes from "./style.module.css"
const LessonsList = ({ course }) => {
    console.log("course", course);
    const lessonContext = useContext(LessonContext)
    return <List
        itemLayout="horizontal"
        dataSource={course.lessons}
        renderItem={(item, index) => (
            index == lessonContext.state.chosenIndex ?
                <List.Item className={classes.itemactive}
                    style={{ borderRadius: 15, padding: 15 }}
                >
                    {< List.Item.Meta
                        avatar={
                            < Avatar
                                shape="round"
                                style={lessonContext.isWatch(index) ?
                                    {
                                        backgroundColor: "white",
                                    }
                                    :
                                    {
                                        backgroundColor: "grey"
                                    }
                                } >
                                <span style={{ color: "#1890ff" }}>{index + 1}</span>
                            </Avatar >
                        }
                        title={<a style={{ color: "white" }}
                            onClick={() => {
                                lessonContext.selectLesson(index, course.id, item.video)
                            }}> {item.title}</a >
                        }
                        description={
                            <span style={{ color: "white" }}>{item.time + " цаг"}</span>
                        }
                    />}
                </List.Item > :
                <List.Item
                    onClick={() => {
                        lessonContext.selectLesson(index, course.id, item.video)
                    }}
                    className={classes.item}
                    style={{ borderRadius: 15, padding: 15 }}>
                    {< List.Item.Meta
                        avatar={<Avatar
                            className={classes.avatar}
                            shape="round"
                            style={lessonContext.isWatch(index) ?
                                { backgroundColor: "#1890ff", }
                                :
                                { backgroundColor: "grey" }} >
                            {index + 1}
                        </Avatar >}
                        title={< a onClick={() => {
                            lessonContext.selectLesson(index, course.id, item.video)
                        }}> {item.title}</a >}
                        description={item.time + " цаг"}
                    />}
                </List.Item >
        )}></List >
}
export default LessonsList