import { Form, Input, Button, Card, Typography, Row } from 'antd';
import Layout from 'antd/lib/layout/layout';
import { useContext, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import { AuthContext } from '../../context/AuthContext';
import Notification from '../../utils/notification';
const Register = () => {
    const authContext = useContext(AuthContext)
    let navigate = useNavigate();
    const onFinish = (values) => {
        if (values.password === values.rePassword) {
            authContext.register(values);
        } else {
            Notification("warning", "Анхааруулага", "Оруулсан нууц үгүүд хоорондоо таарахгүй байна");
        }
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    useEffect(() => {
        if (authContext.state.logged) {
            navigate("/");
        }
    }, [])

    return (
        <Layout style={{ "display": "flex", "alignItems": "center", height: "100vh", paddingTop: "5%" }}>
            <Card style={{ maxWidth: 700, width: 800 }}>
                <Typography style={{ "display": "flex", justifyContent: "center", fontSize: "32px", fontStyle: "bold", marginBottom: 40 }}>Бүртгүүлэх</Typography>
                <Form
                    name="basic"
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 16,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        label="И-мэйл"
                        name="email"
                        rules={[
                            {
                                validator: async (_, email) => {
                                    if (!/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(email)) {
                                        return Promise.reject(new Error('Имэйл хаяг шаардлага хангахгүй байна'));
                                    }
                                },
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Нууц үг"
                        name="password"
                        rules={[
                            {
                                validator: async (_, password) => {
                                    if (!password || password.length < 6) {
                                        {
                                            return Promise.reject(new Error('Нууц үг хамгийн багадаа 6 тэмдэгт байна'));
                                        }
                                    }
                                }
                            }
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    <Form.Item
                        label="Нууц давтах"
                        name="rePassword"
                        rules={[
                            {
                                validator: async (_, rePassword) => {
                                    if (!rePassword || rePassword.length < 6) {
                                        {
                                            return Promise.reject(new Error('Нууц үг хамгийн багадаа 6 тэмдэгт байна'));
                                        }
                                    }
                                }
                            }
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    <Row style={{ "display": "flex", justifyContent: "end" }}>
                        <a onClick={() => {
                            navigate(`/login`);
                        }}>Нэвтрэх</a>
                    </Row>
                    <Form.Item
                        wrapperCol={{
                            offset: 8,
                            span: 16,
                        }}
                    >
                        <Button type="primary" htmlType="submit" loading={authContext.state.loading}>
                            Бүртгүүлэх
                        </Button>

                    </Form.Item>
                </Form>
            </Card>
        </Layout>
    );
};
export default Register;