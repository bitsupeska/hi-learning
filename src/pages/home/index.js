import { Row } from 'antd'
import Layout, { Content } from 'antd/lib/layout/layout'
import { Typography, Input } from 'antd';
import { Divider } from 'antd';
import React, { useContext } from 'react'
import { ProductContext } from '../../context/ProductContext';
import Product from '../Product/product';
export default function Index() {
    const productContext = useContext(ProductContext)
    const { Title } = Typography;
    const { Search } = Input;
    const onSearch = (value) => {
        productContext.searchCourse(value);
    }
    return (
        <Layout className="site-layout">
            <div className="site-layout-background" style={{ padding: 24, textAlign: 'center' }}>
                <Title>Бүх сургалтууд</Title>
                <Content style={{ padding: '0 50px', height: "100vh" }} >
                    <div>
                        <Divider />
                        <Search placeholder="Хайх" allowClear onSearch={onSearch} style={{ width: 200 }} />
                        <Divider dashed />
                        <Row justify="center" align="top">

                            <Product></Product>
                        </Row>
                    </div>
                </Content >
            </div>
        </Layout>
    )
}
