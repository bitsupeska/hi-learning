import { Col, Empty, Row } from 'antd';
import { useContext } from 'react';
import BasketList from "../../components/basket/components/list"
import { BasketContext } from '../../context/BasketContext';
import Footer from './components/footer';
const CheckOut = () => {
    const basketContext = useContext(BasketContext)
    return <Row >
        <Col span={8} offset={8} style={{
            padding: "20px", boxShadow: "0px 4px 8px 3px rgba(0,0,0,0.2)",
            transition: "0.3s"
        }}>
            {basketContext.state.basketCourses.length > 0 ?
                <BasketList basketContext={basketContext} footer={
                    <Footer basketContext={basketContext}>
                    </Footer>}>

                </BasketList> :
                <Empty image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                    description={
                        <span>
                            Сагс хоосон байна
                        </span>
                    }>
                </Empty>}

        </Col>
    </Row>

}

export default CheckOut;