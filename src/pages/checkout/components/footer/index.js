import { MoneyCollectOutlined } from '@ant-design/icons/lib/icons';
import { Button, Card, Form, Input, Typography } from 'antd';
import React from 'react';
import InputMask from 'react-input-mask';

class Cart extends React.Component {
    state = {
        cartNumber: '',
        endDate: '',
        cvv: '',
    }

    onChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    onFinish(value) {
        console.log("value", value)
    }
    onFinishFailed(error) {
        console.log("error", error)
    }
    render() {
        return <Card title={"Нийт дүн : " + this.props.basketContext.sumPrice() + "₮"}>

            <Form
                name="basic1"
                labelCol={{ span: 8 }}
                wrapperCol={{ span: 16 }}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="Картын дугаар"
                    name="cartNumber"
                    rules={[
                        {
                            validator: async (_, cardNumber) => {
                                if (!cardNumber || cardNumber.includes("_")) {
                                    return Promise.reject(new Error('Картын дугаар бүрэн оруулна уу'));
                                }
                            },
                        },
                    ]}
                >
                    <InputMask className='ant-input'
                        mask="9999 9999 9999 9999" maskChar={"_"} placeholder='---- ---- ---- ----' value={this.state.cartNumber} onChange={this.onChange} />
                </Form.Item>
                <Form.Item
                    label="Дуусах огноо"
                    name="endDate"

                    rules={[
                        {
                            validator: async (_, endDate) => {
                                if (!endDate || endDate.includes("_")) {
                                    return Promise.reject(new Error('Картын дуусах огноо бүрэн оруулна уу'));
                                }
                            },
                        },
                    ]}
                >
                    <InputMask className='ant-input' mask="9999/99" maskChar={"_"} placeholder='YYYY/MM' value={this.state.endDate} onChange={this.onChange} />
                </Form.Item>

                <Form.Item
                    label="CVV"
                    name="cvv"

                    rules={[
                        {
                            validator: async (_, cvv) => {
                                if (!cvv || cvv.includes("_")) {
                                    return Promise.reject(new Error('Картын дуусах огноо бүрэн оруулна уу'));
                                }
                            },
                        },
                    ]}
                >
                    <InputMask className='ant-input' mask="999" maskChar={"_"} placeholder='CVV' value={this.state.endDate} onChange={this.onChange} />
                </Form.Item>
                <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                    <Button block icon={<MoneyCollectOutlined></MoneyCollectOutlined>}
                        type="primary"
                        onClick={() => {
                            this.props.basketContext.order();
                        }}
                        loading={this.props.basketContext.state.orderLoading}
                        htmlType="submit">
                        Худалдаж авах
                    </Button>
                </Form.Item>
            </Form>
        </Card>

    }
}
export default Cart