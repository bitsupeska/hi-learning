import { Form, Input, Button, Card, Typography, Row } from 'antd';
import Layout from 'antd/lib/layout/layout';
import { useContext, useEffect } from 'react';

import { useNavigate } from "react-router-dom";
import { AuthContext } from '../../context/AuthContext';
const Login = () => {
    let navigate = useNavigate();
    const authContext = useContext(AuthContext);
    const onFinish = (values) => {
        console.log('Success:', values);
        authContext.login(values)
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    useEffect(() => {
        if (authContext.state.logged) {
            navigate("/");
        }
    }, [])

    return (
        <Layout style={{ "display": "flex", "alignItems": "center", height: "100vh", paddingTop: "5%" }}>
            <Card style={{ maxWidth: 700, width: 800 }}>
                <Typography style={{ "display": "flex", justifyContent: "center", fontSize: "32px", fontStyle: "bold", marginBottom: 40 }}>Нэвтрэх</Typography>
                <Form
                    name="basic"
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 16,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        label="И-мэйл"
                        name="email"
                        // rules={[{ required: true }, { type: 'email', warningOnly: true }]}
                        rules={[
                            {
                                validator: async (_, email) => {
                                    if (!/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(email)) {
                                        return Promise.reject(new Error('Имэйл хаяг шаардлага хангахгүй байна'));
                                    }
                                },
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Нууц үг"
                        name="password"
                        // rules={[{ required: true }, { type: 'password', warningOnly: true }, { type: 'string', min: 6 }]}
                        rules={[
                            {
                                validator: async (_, password) => {
                                    if (!password || password.length < 6) {
                                        {
                                            return Promise.reject(new Error('Нууц үг хамгийн багадаа 6 тэмдэгт байна'));
                                        }
                                    }
                                }
                            }
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    <Row style={{ "display": "flex", justifyContent: "end" }}>
                        <a onClick={() => { navigate(`/register`); }}>Бүртгүүлэх</a>
                    </Row>
                    <Form.Item
                        wrapperCol={{
                            offset: 8,
                            span: 16,
                        }}
                    >
                        <Button type="primary" htmlType="submit" loading={authContext.state.loading}>
                            Нэвтрэх
                        </Button>

                    </Form.Item>
                </Form>
            </Card>
        </Layout>
    );
};
export default Login;