import { Col, Empty, Spin } from "antd"
import { useContext, useEffect } from "react";
import { ProductContext } from "../../../../context/ProductContext";
import ProductItem from "../../../Product/components/item/product_item"
const MyCourseList = () => {
    const productContext = useContext(ProductContext);
    useEffect(() => {
        productContext.getDatas();
    }, [])
    const myCourses = productContext.state.courses.filter(couse => couse.ordered);
    return productContext.state.loading ?
        <Spin tip="Уншиж байна..."></Spin> :
        (myCourses.length > 0 ? myCourses.map(item => {
            return <Col key={item.id} span={5} style={{ margin: "1%", }}  >
                <ProductItem item={item}></ProductItem>
            </Col>
        }) : <Empty
            image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
            imageStyle={{
                minHeight: 200,
            }}
            description={
                <span>
                    Мэдээлэл байхгүй байна
                </span>
            }
        >
        </Empty>)
}

export default MyCourseList