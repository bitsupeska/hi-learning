import { Row } from "antd";
import Layout, { Content } from "antd/lib/layout/layout";
import MyCourseList from "./components/list";
import { Typography, Input } from 'antd';
import { Divider } from 'antd';
import { ProductContext } from "../../context/ProductContext";
import { useContext } from "react";

const MyCourses = () => {

    const { Title } = Typography;
    const { Search } = Input;
    const productContext = useContext(ProductContext)
    const onSearch = (value) => {
        productContext.searchCourse(value);
    }
    return (
        <Layout className="site-layout">
            <div className="site-layout-background" style={{ padding: 24, textAlign: 'center' }}>
                <Title>Миний сургалтууд</Title>
                <Content style={{ padding: '0 50px', height: "100vh" }} >
                    <div>
                        <Divider />
                        <Search placeholder="Хайх" allowClear onSearch={onSearch} style={{ width: 200 }} />
                        <Divider dashed />
                        <Row justify="center" align="top">
                            <MyCourseList></MyCourseList>
                        </Row>
                    </div>
                </Content >
            </div>
        </Layout>
    )
}
export default MyCourses;