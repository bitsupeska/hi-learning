import { DeleteOutlined, PlayCircleOutlined, ShoppingCartOutlined } from "@ant-design/icons/lib/icons";
import { Button } from "antd";

import { useNavigate } from "react-router-dom";
const BasketButton = ({ item, basketContext }) => {
    let navigate = useNavigate();
    return <div>
        <Button onClick={(e) => {

            e.stopPropagation();
            if (item.ordered) {
                localStorage.setItem("watchCourse", JSON.stringify(item))
                navigate(`lessons/${item.id}`)
            } else {
                basketContext.insertProduct(item);
            }
        }}
            block={!basketContext.checkHaveProduct(item.id)}
            type={basketContext.checkHaveProduct(item.id) ? "default" : "primary" }
            style={{ marginTop: "4%" }}
            icon={item.ordered ? <PlayCircleOutlined></PlayCircleOutlined> : <ShoppingCartOutlined></ShoppingCartOutlined>}>
            {item.ordered ? "Үзэх" : basketContext.checkHaveProduct(item.id) ? "Сагсанд хийсэн" : "Сагсанд хийх"}
        </Button>
        {basketContext.checkHaveProduct(item.id) &&
            <Button
                onClick={(e) => {
                    e.stopPropagation(); basketContext.deleteProduct(item.id)
                }}>
                <DeleteOutlined
                    style={{ color: "red" }}>
                </DeleteOutlined>
            </Button>}
    </div>
}
export default BasketButton;
