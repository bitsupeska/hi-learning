import { ShoppingCartOutlined } from "@ant-design/icons/lib/icons";
import { Button, Card, Typography, Row, Image } from "antd"
import { useContext } from "react";
import { ProductContext } from "../../../../context/ProductContext";
import { BasketContext } from "../../../../context/BasketContext";
import BasketButton from "../basket_button";
const Product = ({ item }) => {
    const productContext = useContext(ProductContext);
    const basketContext = useContext(BasketContext);

    return <Card
        onClick={() => {
            productContext.selectItem(item.id);
        }}
        title={item.title}
        hoverable
        cover={
            <Image
                onClick={(e) => {
                    e.stopPropagation()
                }}
                preview={{
                    src: item.image,
                }}
                height={200} src={item.image} ></Image>
        }
    >
        <Typography style={{ "fontSize": "20px", fontWeight: "bold" }}>{item.price}₮</Typography>
        <Row style={{

            display: "flex",
            justifyContent: "space-between",
            marginTop: "4%"
        }}>
            <Typography>Нийт хичээл<br></br> {item.unitCount}</Typography>
            <Typography>Нийт хугацаа<br></br> {item.unitTime}</Typography>
        </Row>
        <BasketButton item={item} basketContext={basketContext}></BasketButton>
        <Row style={{
            display: "flex",
            marginTop: "4%",
            justifyContent: "space-between"
        }}>

            <Typography>Багш:{item&& item.teacher&&item.teacher.name}</Typography>
            <a onClick={(e) => {

                e.stopPropagation();
                productContext.selectItem(item.id);
            }}>Дэлгэрэнгүй</a>
        </Row>
    </Card>
}

export default Product;