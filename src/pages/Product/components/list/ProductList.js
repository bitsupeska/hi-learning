import React, { useContext, useEffect } from 'react'
import { Col, Empty, Spin } from 'antd';//Space,
import { ProductContext } from '../../../../context/ProductContext';
import ProductItem from '../item/product_item';
export default function ListItems() {
    const productContext = useContext(ProductContext);
    useEffect(() => {
        productContext.getDatas();
    }, [])
    return (
        productContext.state.loading ?
            <Spin tip="Уншиж байна..."></Spin> :
            (productContext.state.courses.length > 0 ? productContext.state.courses.map(item => {
                return <Col key={item.id} span={5} style={{ margin: "1%", }}  >
                    <ProductItem item={item}></ProductItem>
                </Col>
            }) : <Empty
                image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                imageStyle={{
                    minHeight: 200,
                }}
                description={
                    <span>
                        Мэдээлэл байхгүй байна
                    </span>
                }
            >
            </Empty>)
    )
}
