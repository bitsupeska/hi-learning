


import BarLoader from "react-spinners/BarLoader";
import { PageHeader,  Descriptions, Image, Typography, Row } from 'antd';
import { useContext, useEffect } from 'react';
import { BasketContext } from '../../../../context/BasketContext';
import { ProductContext } from '../../../../context/ProductContext';
import BasketButton from '../basket_button';
import Lessons from '../lessons';

// import { useNavigate } from "react-router-dom";
const ProductDetail = () => {
    // const navigate = useNavigate();
    const productContext = useContext(ProductContext);
    const basketContext = useContext(BasketContext);
    const product = productContext.state.course;
    useEffect(() => {
        productContext.getCourse(localStorage.getItem("productId"))
    }, [])


    return productContext.state.courseLoading ? <div
        className="site-layout-background"
        style={{

            justifyContent: "center",
            display: "flex",
        }}><BarLoader width={"100%"} color={"#1890ff"}></BarLoader></div> : <div className="site-page-header-ghost-wrapper">
        <PageHeader
            ghost={false}
            onBack={() => {
                window.history.back()
            }}
            title={product.title}
            extra={[
                <BasketButton item={product} basketContext={basketContext}></BasketButton>
            ]}
        >
            <Descriptions size="small" column={3}>
                <Descriptions.Item label="Үүсгэсэн огноо"><a>{product.createdDate}</a></Descriptions.Item>
                <Descriptions.Item label="Үнэ">
                    <a>{product.price}₮</a>
                </Descriptions.Item>
                <Descriptions.Item label="Нийт хичээлийн тоо"><a>{product.unitCount}</a></Descriptions.Item>
                <Descriptions.Item label="Нийт хичээлийн цаг"><a>{product.unitTime}</a></Descriptions.Item>
                <Descriptions.Item label="Заах багш">
                    <a>
                        {product.teacher.name}
                    </a>
                </Descriptions.Item>
            </Descriptions>
            <Row style={{ display: "flex", justifyContent: "center" }}>
                <Row><Image width={500} src={product.image}></Image></Row>

            </Row>
            <Row style={{ display: "flex", justifyContent: "center" }}><Typography style={{ fontSize: 20, fontWeight: "bold" }}>Хичээлүүд</Typography></Row>
            <Row style={{ display: "flex", justifyContent: "center" }}>
                <Lessons items={product.lessons}></Lessons>
            </Row>
        </PageHeader>
    </div >
}

export default ProductDetail;