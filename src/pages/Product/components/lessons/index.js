import { Steps } from "antd";

const Lessons = ({ items }) => {
    const { Step } = Steps;

    return <Steps style={{ display: "flex", width: "50vh" }} direction="vertical" current={0}>
        {
            items.map(item => {
                return <Step title={item.title} description={item.time} />
            })
        }
    </Steps>
}

export default Lessons;