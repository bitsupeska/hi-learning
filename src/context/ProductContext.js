import React, { useContext, useState } from "react";
import ApiService from '../http/axios';
import { useNavigate } from "react-router-dom";
import { BasketContext } from "./BasketContext";
const initialState = {
    courses: [],
    course: null,
    courseLoading: true,
    loading: false,
};


export const ProductContext = React.createContext();

export const ProductProvider = (props) => {
    const navigate = useNavigate();
    const basketContext = useContext(BasketContext);
    const [state, setState] = useState(initialState);
    const getDatas = async () => {
        setState({ ...state, loading: true });
        ApiService("get", "/courses.json").then(res => {//{ limit: 50 }

            getOrderedProducts(res, Object.keys(res));

        })
            .catch(err => {
                console.log("err", err)
                setState({ courses: [], error: err })
            })
    }
    const getIds = (arr) => { //zahialsn course-uudin id -g avj baiga(olon id)
        let result = [];
        let temp = arr.map(item => {
            return item.courses
        })
        for (let i = 0; i < temp.length; i++) {
            for (let j = 0; j < temp[i].length; j++) {
                result.push(temp[i][j])
                console.log("return", temp[i][j]);

            }
        }

        return result;
    }
    const searchCourse = (value) => {
        setState({ ...state, loading: true });
        let url = value ? `orderBy="title"&equalTo="${value}"` : ""
        ApiService("get", `/courses.json?${url} `).then(res => {
            getOrderedProducts(res, Object.keys(res));
        })
            .catch(err => {
                setState({ courses: false, error: err })
            })
    }
    const getOrderedProducts = (res, keys) => {//heregelegchiin zahialsan coursuudiin medeelel avah
        let tempData = Object.values(res);


        for (let i = 0; i < tempData.length; i++) {
            tempData[i].id = Object.keys(res)[i];
        }
        const localId = localStorage.getItem("localId") 
        if (localId) { // hereglech nevtersn 
            ApiService("get", `/orders.json?orderBy="localId"&equalTo="${localId}"`).then(responce => { // hereglechin zahialga buh datag avch ireh 
                const orderKeys = getIds(Object.values(responce)); //zahialsn buh id avj irn
                console.log("orderKeys", orderKeys)
                for (let i = 0; i < keys.length; i++) {
                    tempData[i].ordered = orderKeys.indexOf(keys[i]) > -1 ? true : false; // zahialsn ugui temp datad nemj ugj bga

                }
                var storedBasket = JSON.parse(localStorage.getItem("basket"));
                console.log("storedBasket", storedBasket);
                if (storedBasket) { // sagsand dahi medeellig shinechleh ,uzsen hichel hasaj bga 
                    const tempBasket = [];
                    for (let i = 0; i < storedBasket.length; i++) {
                        if (orderKeys.indexOf(storedBasket[i].id) < 0) {
                            tempBasket.push(storedBasket[i]);
                            // const tempData = storedBasket.filter(item => item.id !== storedBasket[i].id);
                            // localStorage.setItem("basket", JSON.stringify(tempData));
                        }
                    }
                    console.log("orderKeys", orderKeys)
                    console.log("tempBasket", tempBasket)
                    basketContext.replaceProduct(tempBasket)

                }
                setState({ courses: [...tempData], loading: false })
            }).catch(err => {

                setState({ courses: [], loading: false, error: err })
            })
        } else {
            setState({ courses: [...tempData], loading: false })
        }
    }
    const selectItem = (id) => {
        setState({ ...state, courseLoading: true });
        localStorage.setItem("productId", id);
        navigate("productDetail")
    }
    const getCourse = (id) => {
        console.log("orloo", state.courses)
        setState({ ...state, courseLoading: true });

        const localId = localStorage.getItem("localId")
        ApiService("get", `/courses/${id}.json `).then(res => {
            let tempData = res;
            ApiService("get", `/orders.json?orderBy="localId"&equalTo="${localId}"`).then(responce => {
                const orderKeys = getIds(Object.values(responce));
                tempData.ordered = orderKeys.indexOf(id) > -1 ? true : false;
                setState({ ...state, course: { ...tempData, id }, courseLoading: false })
            }).catch(err => {

                setState({ ...state, course: null, courseLoading: false, error: err })
            })
        })
            .catch(err => {
                setState({ ...state, course: null, courseLoading: false, error: err })
            })
    }

    return (
        <ProductContext.Provider
            value={{
                state,
                getDatas,
                searchCourse,
                getCourse,
                selectItem
            }}
        >
            {props.children}
        </ProductContext.Provider>
    );
};
