import React, { useEffect, useState } from "react";
import ApiService from '../http/axios';
import { useNavigate } from "react-router-dom";
import { message } from "antd";
const initialState = {
    url: null,
    loading: true,
    watchList: [],
    selectItem: false,
    chosenIndex: 0,
    error: null,
    id: null
};


export const LessonContext = React.createContext();

export const LessonProvider = (props) => {
    const navigate = useNavigate();
    const localId = localStorage.getItem("localId");

    const [state, setState] = useState(initialState);

    const course = JSON.parse(localStorage.getItem("watchCourse"));
    const getLessons = (productId) => { //hicheludig avj irj baiga
        setState({ watchList: [], loading: true, error: null })
        ApiService("get", `/userLessons/${localId}/${productId}.json?"`).then(responce => {
            if (responce) {
                setState({ watchList: responce, loading: false, error: null, url: course.lessons[responce[responce.length - 1]].video, chosenIndex: responce[responce.length - 1] })
            } else {
                setState({ watchList: [], loading: false, error: null, url: course.lessons[0].video, chosenIndex: 0 })
            }

        }).catch(err => {
            setState({ watchList: [], loading: false, error: err, url: null })
        })
    }
    const selectLesson = (index, productId) => { //hichel uzj baiga
        setState({ watchList: [], loading: true, error: null, url: null })

        let tempArray = [...state.watchList];
        const findIndex = state.watchList.indexOf(index)
        if (findIndex > -1) {
            // setTimeout(() => {
            //     setState({ ...state, loading: false, error: null, selectItem: true, url: course.lessons[index].video })
            // }, 1000)
            if (findIndex != tempArray.length - 1) {
                const temp = tempArray[findIndex];
                tempArray[findIndex] = tempArray[tempArray.length - 1];
                tempArray[tempArray.length - 1] = temp;
            }
            ApiService("put", `/userLessons/${localId}/${productId}.json?"`, (tempArray)).then(responce => {
                if (responce) {
                    setState({ watchList: tempArray, loading: false, error: null, selectItem: true, url: course.lessons[index].video, chosenIndex: index })
                } else {
                    setState({ watchList: [], loading: false, error: null, url: null })
                }

            }).catch(err => {
                setState({ watchList: [], loading: false, error: err, url: null })
            })

        } else {
            tempArray.push(index);
            ApiService("put", `/userLessons/${localId}/${productId}.json?"`, (tempArray)).then(responce => {
                if (responce) {
                    setState({ watchList: tempArray, loading: false, error: null, selectItem: true, url: course.lessons[index].video, chosenIndex: index })
                } else {
                    setState({ watchList: [], loading: false, error: null, url: null })
                }

            }).catch(err => {
                setState({ watchList: [], loading: false, error: err, url: null })
            })
        }
    }

    const isWatch = (index) => { // index es orj irsn hichelig shalgaj baiga
        if (index === 0) {
            return true;
        } else {
            console.log("watch", state.watchList.indexOf(index))
            if (state.watchList.indexOf(index) > -1) {
                return true;
            } else {
                return false;
            }
        }
    }

    return (
        <LessonContext.Provider
            value={{
                state,
                selectLesson,
                getLessons,
                isWatch
            }}
        >
            {props.children}
        </LessonContext.Provider>
    );
};
