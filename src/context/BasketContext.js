import React, { useContext, useEffect, useState } from "react";
import ApiService from '../http/axios';
import { useNavigate } from "react-router-dom";
import { message } from "antd";
import { AuthContext } from "./AuthContext";
const initialState = {
    basketCourses: [],
    loading: false,
    visible: false,
    orderLoading: false,
};


export const BasketContext = React.createContext();

export const BasketProvider = (props) => {
    const navigate = useNavigate();
    useEffect(() => {
        var storedBasket = JSON.parse(localStorage.getItem("basket"));
        console.log("storedBasket", storedBasket);
        if (storedBasket)
            setState({ ...state, loading: false, basketCourses: storedBasket });

    }, [])
    const [state, setState] = useState(initialState);
    const getDatas = async () => {
        setState({ ...state, loading: true });
    }
    const show = () => {//drawer neeh
        setState({ ...state, visible: true });

    }
    const hide = () => {//drawer haah
        setState({ ...state, visible: false });

    }
    //product->hicheel
    const insertProduct = (product) => {//sagsand hicheel nemne
        console.log("orood baina");
        if (checkHaveProduct(product.id)) {
            message.info(product.title + '-г сагсанд хийсэн байна');
        } else {
            const tempData = state.basketCourses;
            tempData.push(product);
            setState({ ...state, basketCourses: [...tempData] })
            localStorage.setItem("basket", JSON.stringify(tempData));
            message.success(product.title + '-г сагсанд хийлээ');

        }

    }
    const sumPrice = () => {
        return state.basketCourses.reduce((n, { price }) => parseInt(n) + parseInt(price), 0)
    }

    const deleteProduct = (id) => {

       
        const tempData = state.basketCourses.filter(item => item.id !== id);
        setState({ ...state, basketCourses: [...tempData] })
        localStorage.setItem("basket", JSON.stringify(tempData));
    }

    const replaceProduct = (array) => {//hereglegch sagsandah coursiig zahialsan baival teriig boliulna

        setState({ ...state, basketCourses: [...array] })
        localStorage.setItem("basket", JSON.stringify(array));
    }

    const checkHaveProduct = (id) => {//tuhain id tei course sagsand baigaa esehig shalgah functino
        const product = state.basketCourses.filter(item => item.id === id)//tuhain id tentsuu elemntuudiig butsaana
        if (product && product.length > 0) {//tuhain element baina
            return true;
        } else {

            return false;//tuhain element baina
        }
    }
    const order = () => {//zahialaga hiih service duudaj baigaa heseg
        const localId = localStorage.getItem("localId");
        if (localId) {//nevtreegui hereglech zahialj
            setState({ ...state, orderLoading: true });
            ApiService("post", `/orders.json `,
                {
                    localId: localId,
                    courses: state.basketCourses.map(course => course.id),
                    date: new Date()
                }).then(res => {
                    setState({ ...state, basketCourses: [], orderLoading: false })
                    localStorage.removeItem("basket");
                    navigate("/");
                    message.success("Амжилттай худалдан авлаа");
                })
                .catch(err => {
                    setState({ ...state, orderLoading: false, error: err })
                })
        } else {
            message.info("Захиалгаа нэвтэрч орсны дараа хийнэ үү");
        }

    }
    return (
        <BasketContext.Provider
            value={{
                state,
                getDatas,
                insertProduct,
                show,
                hide,
                checkHaveProduct,
                sumPrice,
                deleteProduct,
                order,
                replaceProduct,
            }}
        >
            {props.children}
        </BasketContext.Provider>
    );
};
