import React, { useState } from "react";
import Notification from "../utils/notification";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { message } from "antd";
const initialState = {
    loading: false,
    error: null,
    localId: null,
    expiresIn: null,
    logged: null,
    email: null,
    refreshToken: null,
    idToken: null,
};


export const AuthContext = React.createContext();

export const AuthProvider = (props) => {
    let navigate = useNavigate();
    const [state, setState] = useState(initialState);

    const login = async (values) => {
        setState({ ...state, loading: true });
        const url = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBr910EOVjzdDiGUQdjhvs9mpEWJ3geYE4
        `;
        axios.post(url, { ...values, returnSecureToken: true }).then(res => {
            console.log("res", res);
            saveUserData(res);
            Notification("success", "Амжилттай", "Нэвтэрлээ")
        }).catch(err => {
            console.log("err", err)
            setState({
                ...state,
                logged: false,
                loading: false,
                localId: null,
                expiresIn: null,
                logged: null,
                email: null,
                refreshToken: null,
                idToken: null,
            });

            if (err && err.response && err.response.data && err.response.data.error && err.response.data.error.message) {
                const msg = err.response.data.error.message === "INVALID_PASSWORD" ? "Нэвтрэх нэр нууц үг буруу байна" : err.response.data.error.message
                Notification("error", "Алдаа", msg)

            } else {
                Notification("error", "Алдаа", navigator.onLine ? "Сервертэй холбогдоход алдаа гарлаа" : "Интернэт холболтоо шалгана уу")
            }
        });
    }
    const autoLogin = () => {
        if (localStorage.getItem("idToken")) {//&& localStorage.getItem("user")
            setState({
                logged: true,
                localId: localStorage.getItem("localId"),
                expiresIn: localStorage.getItem("expiresIn"),
                email: localStorage.getItem("email"),
                refreshToken: localStorage.getItem("refreshToken"),
                idToken: localStorage.getItem("idToken")
            });//, user: JSON.parse(localStorage.getItem("user"))
        }
        else {
            setState({ logged: false });
        }
    }
    const register = (values) => {
        setState({ ...state, loading: true });
        const url = `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyBr910EOVjzdDiGUQdjhvs9mpEWJ3geYE4
        `;
        axios.post(url, { ...values, returnSecureToken: true }).then(res => {
            console.log("res", res);
            saveUserData(res);
            Notification("success", "Амжилттай", "Бүртгүүллээ")
        }).catch(err => {

            setState({ ...state, logged: false, loading: false });
            if (err && err.response && err.response.data && err.response.data.error && err.response.data.error.message) {
                Notification("error", "Алдаа", err.response.data.error.message)

            } else {
                Notification("error", "Алдаа", navigator.onLine ? "Сервертэй холбогдоход алдаа гарлаа" : "Интернэт холболтоо шалгана уу")
            }
        });
    }
    const logOut = () => {
        message.success("Системээс гарлаа");
        setState({ logged: false });
        localStorage.removeItem("localId");
        localStorage.removeItem("expiresIn");
        localStorage.removeItem("email");
        localStorage.removeItem("refreshToken");
        localStorage.removeItem("idToken");
        navigate("/")
        window.location.reload();
    }
    const saveUserData = (res) => {
        localStorage.setItem("localId", res.data.localId);
        localStorage.setItem("expiresIn", res.data.expiresIn);
        localStorage.setItem("email", res.data.email);
        localStorage.setItem("refreshToken", res.data.refreshToken);
        localStorage.setItem("idToken", res.data.idToken);
        setState({
            ...state,
            logged: true,
            loading: false,
            localId: res.data.localId,
            expiresIn: res.data.expiresIn,
            email: res.data.email,
            refreshToken: res.data.refreshToken,
            idToken: res.data.idToken,
        });
        localStorage.setItem("activeMenu", "1");
        navigate(`/`);

    }
    return (
        <AuthContext.Provider
            value={{
                state,
                login,
                autoLogin,
                logOut,
                register,
            }}
        >
            {props.children}
        </AuthContext.Provider>
    );
};
