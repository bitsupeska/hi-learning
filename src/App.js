
import { useContext, useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';
import './App.css';
import MyHeader from "./components/header/MyHeader"
import { AuthContext } from './context/AuthContext';
import HomePage from "./pages/home"
import Login from './pages/login';
import ProductDetail from './pages/Product/components/detail/product_detail';
import Register from './pages/register';
import CheckOut from './pages/checkout';
import Basket from "./components/basket"
import MyCourses from './pages/my_courses';
import Lessons from './pages/lessons';
function App() {
  const authContext = useContext(AuthContext);
  useEffect(() => {
    authContext.autoLogin();
  }, [])
  return (

    <>
      <Basket></Basket>
      <MyHeader></MyHeader>
      <Routes>

        <Route exact path="/" element={<HomePage />}>
        </Route>
        <Route exact path="/login" element={<Login />}>
        </Route>
        <Route exact path="/register" element={<Register />}>
        </Route>
        <Route exact path="/checkOut" element={<CheckOut />}>
        </Route>
        <Route path="/productDetail" element={<ProductDetail />} />
        <Route path="/productDetail/lessons/:id" element={<Lessons />} />
        <Route path="/myCourses" element={<MyCourses />} />
        <Route path="/myCourses/lessons/:id" element={<Lessons />} />
        <Route path="/lessons/:id" element={<Lessons />} />


      </Routes>
    </>
  );
}
{/* <Route index element={<Home />} />
          <Route path="teams" element={<Teams />}>
            <Route path=":teamId" element={<Team />} />
            <Route path="new" element={<NewTeamForm />} />
            <Route index element={<LeagueStandings />} />
          </Route> */}
export default App;
