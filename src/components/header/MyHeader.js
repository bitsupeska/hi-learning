import React from 'react'
import { Button, Col, Dropdown, Menu, message, Row } from 'antd';
import { Header } from 'antd/lib/layout/layout';
import classes from "./style.module.css";
import { useNavigate } from "react-router-dom";
import { useContext } from "react"
import { AuthContext } from '../../context/AuthContext';
import { ShoppingCartOutlined } from "@ant-design/icons/lib/icons"
import { Badge } from "antd";
import { BasketContext } from '../../context/BasketContext';

import Avatar from 'antd/lib/avatar/avatar';
export default function MyHeader() {
    const authContext = useContext(AuthContext);
    const basketContext = useContext(BasketContext);

    let navigate = useNavigate();
    const menu = (
        <Menu>
            <Menu.Item key="1" onClick={() => { navigate('/myCourses') }}> Миний сургалтууд</Menu.Item>
            <Menu.Item key="2" onClick={() => { authContext.logOut(); }}>Гарах</Menu.Item>
        </Menu>
    );

    return (
        <>
            <Header style={{ backgroundColor: "white" }}>

                <Row style={{ display: "flex", justifyContent: "space-between" }}>
                    <Col>
                        <div >{authContext.state.email ? <div style={{ color: "#1890ff", fontWeight: "bold" }}>
                            <span style={{ color: "black" }}>Сайн байна уу : </span>
                            {authContext.state.email} </div> : <div className={classes.title}>Hi Learn</div>}</div>
                    </Col>
                    <Col>
                        <Menu theme="light" mode="horizontal" onSelect={(key) => {
                            localStorage.setItem("activeMenu", key.key)
                        }} defaultSelectedKeys={[localStorage.getItem("activeMenu") ? localStorage.getItem("activeMenu") : '1']}>
                            <Menu.Item onClick={() => {
                                console.log("go home");
                                navigate(`/`);
                            }} key={1}>Нүүр</Menu.Item>

                            <Menu.Item onClick={() => {


                            }} key={2}>Холбоо барих</Menu.Item>
                            <Menu.Item onClick={() => {
                                basketContext.show();

                            }} key={3}> <Badge count={basketContext.state.basketCourses.length} size='small'>
                                    <ShoppingCartOutlined style={{ fontSize: '150%' }}></ShoppingCartOutlined>
                                </Badge></Menu.Item>
                            {authContext.state.logged ?
                                <Menu.Item key={4}><Dropdown overlay={menu}  >
                                    <div >
                                        <div>
                                            <Avatar style={{ background: "#1890ff" }}
                                            >
                                                {authContext.state.email.substring(0, 1).toUpperCase()}
                                            </Avatar>
                                        </div>
                                    </div>
                                </Dropdown>
                                </Menu.Item>

                                :
                                <Menu.Item onClick={() => {
                                    navigate(`/login`);

                                }} key={4}>Бүртгүүлэх | Нэвтрэх</Menu.Item>
                            }
                        </Menu>
                    </Col>
                </Row>
            </Header>
        </>
    )
}
