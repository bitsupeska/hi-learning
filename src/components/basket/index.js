
import { Drawer, Empty } from "antd";
import { useContext } from "react";
import { BasketContext } from "../../context/BasketContext";
import Footer from "./components/footer";
import Lists from "./components/list";
const Basket = () => {
    const basketContext = useContext(BasketContext)
    const onClose = () => {
        basketContext.hide()
    }
    return <Drawer title="Сагс" placement="right" onClose={onClose} visible={basketContext.state.visible}>
        {
            basketContext.state.basketCourses.length > 0 ?
                <Lists basketContext={basketContext} footer={< Footer basketContext={basketContext} ></Footer >}></Lists>
                : <Empty image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"

                    description={
                        <span>
                            Сагс хоосон байна
                        </span>
                    }> </Empty>
        }
    </Drawer >
}
export default Basket;