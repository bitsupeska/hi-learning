import { DeleteFilled } from "@ant-design/icons/lib/icons"
import { Button, List } from "antd"
import Avatar from "antd/lib/avatar/avatar"

const Index = ({ basketContext, footer }) => {
    
    return <List
   
        footer={
            footer
        }
        pagination={{
      onChange: page => {
        console.log(page);
      },
      pageSize: 3,
    }}
        itemLayout="horzintal"
        style={{ heigth: "90vh" }}
        dataSource={basketContext.state.basketCourses}
        renderItem={item => (
            <List.Item
                style={{ display: "flex", alignItems: "center", justifyContent: "center", alignContent: "center", justifyItems: "center" }}
                extra={
                    <Button onClick={() => {
                        basketContext.deleteProduct(item.id)
                    }} style={{ marginBottom: "10px" }} type="default" shape="round" icon={<DeleteFilled style={{ color: "red" }} />}></Button>

                }
            >
                <List.Item.Meta
                    style={{ display: "flex", alignItems: "center", justifyContent: "center" }}
                    avatar={<Avatar src={item.image} />}
                    title={<a>{item.price}₮</a>}
                    description={item.title}
                />
            </List.Item>
        )} >
    </List >
}

export default Index