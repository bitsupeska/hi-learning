import { Button, Card, message, Typography } from "antd"
import { useContext } from "react";
import { useNavigate } from "react-router-dom";
const Footer = ({ basketContext }) => {
    const navigator = useNavigate();
    return <Card>
        <Typography style={{ fontSize: "20px", fontWieght: "bold" }}> Нийт дүн : {basketContext.sumPrice()}₮</Typography>
        <Button onClick={() => {
            const localId = localStorage.getItem("localId");
            if (localId) {
                basketContext.hide(); 
                navigator("checkOut")
            }
            else {
                message.info("Захиалгаа нэвтэрч орсны дараа хийнэ үү");
            }
        }

        } type="primary" style={{ marginTop: "4%" }} block>Захиалах</Button>
    </Card >
}
export default Footer;