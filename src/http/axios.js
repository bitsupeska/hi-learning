import axios from "axios";
// import history from "../utils/history";
import Notification from "../utils/notification";
//api huselt gargaj baiga neg l gartstai
const instance = axios.create({
    baseURL: "https://hi-learn-299ac-default-rtdb.asia-southeast1.firebasedatabase.app",
    // withCredentials: true,
});
// instance.defaults.withCredentials = true;
const ApiService = (method, url, data, headers = {}) => {
    // try {
    // } catch (err) { }
    const options = {
        method: method,
        url: url,
        ...(data?._download && { responseType: "blob" }),

        headers: {
            "Content-Type": "application/json", //'application/octet-stream'
            // Authorization: "Bearer " + localStorage.getItem("token"),
            ...headers,
        },
    };

    if (method.toLowerCase() === "get") options.params = data;
    else options.data = data;

    const onSuccess = function (response) {
        // if (basicOptions !== null) return response
        if (data?._download) {
            //file tatah ued ashiglana duriin file tatdag bolgoh
            // const FileDownload = require("js-file-download");
            // let filename = `report${Math.floor(Math.random() * 100000)}.xlsx`;
            // FileDownload(response.data, filename);
            // return false;
        }
        if (data?.success === false) {
            Notification("error", "Алдаа", data?.message);
            Promise.reject(303 || data?.message);
        }
        return response.data;
    };

    const onError = function (error) {
        //axiosoosoo garsan aldaa
        if (
            error &&
            error.response &&
            error.response.status &&
            error.response.status === 401
        ) {
            if (localStorage.getItem("token")) {
                Notification("error", "Алдаа", "tanii tokenii hugatsaa duussan baina");
                localStorage.removeItem("token"); //ene bolhoor localstorage ashiglaj bgaa bol
                document.cookie =
                    "amazon-token=null; expires=" +
                    new Date(Date.now() - 360 * 24 * 60 * 60 * 1000); //cookie ashiglah ued ingej ustgana
                // setTimeout(() => {
                //     history.push("/");
                //     window.location.reload();
                // }, 1000);
            } else {
                Notification(
                    "error",
                    "Алдаа",
                    error.response.data.error.message
                );
            }
        } else {
            // console.log(error.response.data);
            // console.log("err", error);
            Notification("error", "Алдаа", "Серверийн алдаа");
        }

        // console.error(error)
        // console.error('Request Failed:', error.config)
        // if (error.response) {
        //     console.error('Status:', error.response.status)
        //     console.error('Data:', error.response)
        //     console.error('Headers:', error.response.headers)
        // } else {
        //     console.error('Error Message:', error.message)
        // }

        return Promise.reject(error.response || error.message || "sad");
    };
    // instance.defaults.withCredentials = true;

    return instance(options).then(onSuccess).catch(onError);
};

export default ApiService;